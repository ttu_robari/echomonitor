package ee.taltech.dal;

import ee.taltech.domain.CaptureParameters;
import ee.taltech.dto.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.time.Duration;

@Slf4j
public class Echo360Client {

    private WebClient webClient;
    @Value("${web-client.request.timeout-seconds}")
    private int timeOutInSeconds;

    public Echo360Client(WebClient webClient) {
        this.webClient = webClient;
    }

    public CaptureStatusDto getCaptureStatus() {
        return webClient.get()
                .uri("status/captures")
                .retrieve()
                .bodyToMono(CaptureStatusDto.class)
                .timeout(Duration.ofSeconds(timeOutInSeconds))
                .block();
    }

    public SystemStatusDto getSystemStatus() {
        return webClient.get()
                .uri("status/system")
                .retrieve()
                .bodyToMono(SystemStatusDto.class)
                .timeout(Duration.ofSeconds(timeOutInSeconds))
                .block();
    }

    public MonitoringStatusDto getMonitoringStatus() {
        return webClient.get()
                .uri("status/monitoring")
                .retrieve()
                .bodyToMono(MonitoringStatusDto.class)
                .timeout(Duration.ofSeconds(timeOutInSeconds))
                .block();
    }

    public ScheduleDto getScheduleFile() {
        return webClient.get()
                .uri("diagnostics/system-info/tasks")
                .retrieve()
                .bodyToMono(ScheduleDto.class)
                .timeout(Duration.ofSeconds(timeOutInSeconds))
                .block();
    }

    public byte[] getInputImage(String imageName) {
        return webClient.get()
                .uri("monitoring/{imageName}", imageName)
                .accept(MediaType.IMAGE_JPEG)
                .retrieve()
                .bodyToMono(byte[].class)
                .block();
    }

    public PostResponseDto postRecordCapture() {
        return webClient.post()
                .uri("capture/record")
                .accept(MediaType.TEXT_XML)
                .retrieve()
                .bodyToMono(PostResponseDto.class)
                .timeout(Duration.ofSeconds(timeOutInSeconds))
                .block();
    }

    public PostResponseDto postStopCapture() {
        return webClient.post()
                .uri("capture/stop")
                .accept(MediaType.TEXT_XML)
                .retrieve()
                .onStatus(HttpStatus::isError, response -> Mono.error(new RuntimeException(response.toString())))
                .bodyToMono(PostResponseDto.class)
                .timeout(Duration.ofSeconds(timeOutInSeconds))
                .onErrorResume(e -> Mono.error(new RuntimeException(e.getMessage())))
                .block();
    }

    public PostResponseDto postPauseCapture() {
        return webClient.post()
                .uri("capture/pause")
                .accept(MediaType.TEXT_XML)
                .retrieve()
                .bodyToMono(PostResponseDto.class)
                .timeout(Duration.ofSeconds(timeOutInSeconds))
                .block();
    }

    public PostResponseDto postNewCapture(CaptureParameters newCaptureParameters) {
        return webClient.post()
                .uri("capture/new_capture")
                .accept(MediaType.TEXT_XML)
                .body(BodyInserters.fromPublisher(Mono.just(newCaptureParameters.toString()), String.class))
                .retrieve()
                .bodyToMono(PostResponseDto.class)
                .timeout(Duration.ofSeconds(timeOutInSeconds))
                .block();
    }

    public PostResponseDto postStartMonitoring(CaptureParameters monitoringParameters) {
        return webClient.post()
                .uri("capture/confidence_monitor")
                .accept(MediaType.TEXT_XML)
                .body(BodyInserters.fromPublisher(Mono.just(monitoringParameters.toString()), String.class))
                .retrieve()
                .bodyToMono(PostResponseDto.class)
                .timeout(Duration.ofSeconds(timeOutInSeconds))
                .block();
    }

    public PostResponseDto postExtendCapture(CaptureParameters captureParameters) {
        return webClient.post()
                .uri("capture/extend")
                .accept(MediaType.TEXT_XML)
                .body(BodyInserters.fromPublisher(Mono.just(captureParameters.toString()), String.class))
                .retrieve()
                .bodyToMono(PostResponseDto.class)
                .timeout(Duration.ofSeconds(timeOutInSeconds))
                .block();
    }
}
