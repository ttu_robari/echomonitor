package ee.taltech.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MonitoringStatus {
    private String state;
    private Double volume;
    private List<String> thumbnailImageFileNames;
}
