package ee.taltech.domain;

public enum PostCommand {
    STOP, PAUSE, RECORD, EXTEND, NEW_CAPTURE, START_MONITORING
}
