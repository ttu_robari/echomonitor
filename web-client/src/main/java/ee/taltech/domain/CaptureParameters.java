package ee.taltech.domain;

import lombok.*;
import org.springframework.util.StringUtils;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class CaptureParameters{
    private String description;
    private String captureProfileName;
    private Integer durationInSeconds;

    @Builder
    public CaptureParameters(int durationInSeconds) {
        this.durationInSeconds = durationInSeconds;
    }

    @Override
    public String toString() {
        if (StringUtils.isEmpty(description) && StringUtils.isEmpty(captureProfileName)) {
            return String.format("duration=\"%s\"", durationInSeconds);
        } else {
            return String.format("description=\"%s\"&duration=\"%s\"&capture_profile_name=%s",
                    description, durationInSeconds, captureProfileName);
        }
    }
}
