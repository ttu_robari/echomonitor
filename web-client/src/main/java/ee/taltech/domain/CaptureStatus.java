package ee.taltech.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CaptureStatus {
    private String nextCaptureTitle;
    private LocalDateTime nextCaptureStartTime;
    private String nextCaptureProfile;
    private Integer nextCaptureDurationSeconds;
    private String currentCaptureTitle;
    private LocalDateTime currentCaptureStartTime;
    private String currentCaptureProfile;
    private Integer currentCaptureDurationSeconds;
    private String monitoringProfile;
    private String captureProfile;
    private Boolean isCaptureDetailsPresent;
    private String state;
}
