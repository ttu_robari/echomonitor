package ee.taltech.bll.service;

import ee.taltech.bll.mapper.ScheduleMapper;
import ee.taltech.dal.Echo360Client;
import ee.taltech.domain.Schedule;
import ee.taltech.domain.ScheduleRow;
import ee.taltech.dto.ScheduleDto;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class ScheduleService {

    private final Echo360Client echo360Client;

    public ScheduleService(Echo360Client echo360Client) {
        this.echo360Client = echo360Client;
    }

    public Schedule getSchedule() {
        Schedule schedule = ScheduleMapper.mapScheduleDto(echo360Client.getScheduleFile());
        filterSchedule(schedule);
        return schedule;
    }

    private void filterSchedule(Schedule schedule) {
        List<ScheduleRow> result = new ArrayList<>();
        LocalDate currentDate = LocalDate.now();

        schedule.getScheduleRows().forEach(row -> {
            if (row.getStartTime().toLocalDate().equals(currentDate)) {
                result.add(row);
            }
        });

        schedule.setScheduleRows(result);
    }
}
