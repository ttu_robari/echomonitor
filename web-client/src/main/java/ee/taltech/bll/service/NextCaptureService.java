package ee.taltech.bll.service;

import ee.taltech.bll.mapper.CaptureStatusMapper;
import ee.taltech.dal.Echo360Client;
import ee.taltech.domain.CaptureStatus;
import org.springframework.stereotype.Service;

@Service
public class NextCaptureService {

    private final Echo360Client echo360Client;

    public NextCaptureService(Echo360Client echo360Client) {
        this.echo360Client = echo360Client;
    }

    public CaptureStatus getNextCapture() {
        return CaptureStatusMapper.mapCaptureStatusDto(echo360Client.getCaptureStatus());
    }
}
