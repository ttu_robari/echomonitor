package ee.taltech.bll.service;

import ee.taltech.dal.Echo360Client;
import org.springframework.stereotype.Service;

@Service
public class InputImageService {

    private final Echo360Client echo360Client;

    public InputImageService(Echo360Client echo360Client) {
        this.echo360Client = echo360Client;
    }

    public byte[] getThumbnailImage(String imageName) {
        return echo360Client.getInputImage(imageName);
    }
}
