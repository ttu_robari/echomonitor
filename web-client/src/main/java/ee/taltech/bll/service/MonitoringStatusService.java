package ee.taltech.bll.service;

import ee.taltech.bll.mapper.MonitoringStatusMapper;
import ee.taltech.dal.Echo360Client;
import ee.taltech.domain.MonitoringStatus;
import org.springframework.stereotype.Service;

@Service
public class MonitoringStatusService {

    private final Echo360Client echo360Client;

    public MonitoringStatusService(Echo360Client echo360Client) {
        this.echo360Client = echo360Client;
    }

    public MonitoringStatus getMonitoringStatus() {
        return MonitoringStatusMapper.mapMonitoringStatusDto(echo360Client.getMonitoringStatus());
    }
}
