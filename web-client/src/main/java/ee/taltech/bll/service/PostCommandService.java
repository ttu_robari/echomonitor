package ee.taltech.bll.service;

import ee.taltech.bll.mapper.PostResponseMapper;
import ee.taltech.dal.Echo360Client;
import ee.taltech.domain.CaptureParameters;
import ee.taltech.domain.PostResponse;
import org.springframework.stereotype.Service;

@Service
public class PostCommandService {

    private final Echo360Client echo360Client;

    public PostCommandService(Echo360Client echo360Client) {
        this.echo360Client = echo360Client;
    }

    public PostResponse recordCapture() {
        return PostResponseMapper.mapPostResponseDto(echo360Client.postRecordCapture());
    }

    public PostResponse stopCapture() {
        return PostResponseMapper.mapPostResponseDto(echo360Client.postStopCapture());
    }

    public PostResponse pauseCapture() {
        return PostResponseMapper.mapPostResponseDto(echo360Client.postPauseCapture());
    }

    public PostResponse newCapture(CaptureParameters parameters) {
        return PostResponseMapper.mapPostResponseDto(echo360Client.postNewCapture(parameters));
    }

    public PostResponse startMonitoring(CaptureParameters parameters) {
        return PostResponseMapper.mapPostResponseDto(echo360Client.postStartMonitoring(parameters));
    }

    public PostResponse extendCapture(CaptureParameters parameters) {
        return PostResponseMapper.mapPostResponseDto(echo360Client.postExtendCapture(parameters));
    }
}
