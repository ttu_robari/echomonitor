package ee.taltech.bll.mapper;

import ee.taltech.domain.PostResponse;
import ee.taltech.dto.PostResponseDto;

public class PostResponseMapper {

    private PostResponseMapper() {
        throw new IllegalStateException("Utility class");
    }

    public static PostResponse mapPostResponseDto(PostResponseDto postResponseDto) {
        return new PostResponse(postResponseDto.getText());
    }
}
