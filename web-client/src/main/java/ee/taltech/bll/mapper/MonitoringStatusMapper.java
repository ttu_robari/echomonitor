package ee.taltech.bll.mapper;

import ee.taltech.domain.MonitoringStatus;
import ee.taltech.dto.MonitoringStatusDto;
import ee.taltech.dto.components.ChannelDto;
import ee.taltech.dto.components.SourceDto;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class MonitoringStatusMapper {

    private MonitoringStatusMapper() {
        throw new IllegalStateException("Utility class");
    }

    public static MonitoringStatus mapMonitoringStatusDto(MonitoringStatusDto input) {
        MonitoringStatus output = new MonitoringStatus();
        output.setState(input.getState());
        output.setVolume(Double.NaN);
        output.setThumbnailImageFileNames(new ArrayList<>());

        if (input.getSources() != null) {
            List<SourceDto> sourceList = input.getSources();
            output.setVolume(getAverageAudioPeakFrom(sourceList));
            output.setThumbnailImageFileNames(getThumbnailNamesFrom(sourceList));
        }
        return output;
    }

    private static List<String> getThumbnailNamesFrom(List<SourceDto> sourceList) {
        List<String> resultList = new ArrayList<>();
        for (SourceDto source : sourceList) {
            if (!StringUtils.isEmpty(source.getThumbnail())) {
                resultList.add(source.getThumbnail());
            }
        }
        return resultList;
    }

    private static double getAverageAudioPeakFrom(List<SourceDto> sourceList) {

        double singleSourceAveragePeak;
        double averageAudioPeak = 0;
        double audioSourceCount = 0;

        for (SourceDto source : sourceList) {
            if (source.getChannels() != null && !CollectionUtils.isEmpty(source.getChannels())) {
                singleSourceAveragePeak = 0;
                for (ChannelDto channel : source.getChannels()) {
                    singleSourceAveragePeak += channel.getPeak();
                }
                singleSourceAveragePeak = singleSourceAveragePeak / source.getChannels().size();
                averageAudioPeak += singleSourceAveragePeak;
                audioSourceCount++;
            }
        }

        return audioSourceCount == 0 ? averageAudioPeak : averageAudioPeak / audioSourceCount;
    }
}
