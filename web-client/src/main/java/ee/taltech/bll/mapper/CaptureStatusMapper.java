package ee.taltech.bll.mapper;

import ee.taltech.domain.CaptureStatus;
import ee.taltech.dto.CaptureStatusDto;
import ee.taltech.dto.components.CurrentCaptureDto;
import ee.taltech.dto.components.ScheduledCaptureDto;

import static ee.taltech.bll.helper.TimeZoneAdjustment.covertToSystemTimeOffset;

public class CaptureStatusMapper {

    private CaptureStatusMapper() {
        throw new IllegalStateException("Utility class");
    }

    public static CaptureStatus mapCaptureStatusDto(CaptureStatusDto input) {
        ScheduledCaptureDto nextCapture = input.getNextCapture();
        CurrentCaptureDto currentCapture = input.getCurrentCapture();

        return CaptureStatus
                .builder()
                .nextCaptureTitle(nextCapture.getCaptureParameters().getTitle())
                .nextCaptureProfile(nextCapture.getCaptureParameters().getCaptureProfile().getName())
                .nextCaptureStartTime(nextCapture.getStartTime() != null ?
                        covertToSystemTimeOffset(nextCapture.getStartTime()) : null)
                .nextCaptureDurationSeconds(nextCapture.getDuration())
                .currentCaptureTitle(currentCapture.getScheduleDetails().getCaptureParameters().getTitle())
                .currentCaptureProfile(currentCapture.getScheduleDetails().getCaptureParameters().getCaptureProfile().getName())
                .currentCaptureStartTime(currentCapture.getStartTime() != null ?
                        covertToSystemTimeOffset(currentCapture.getStartTime()) : null)
                .currentCaptureDurationSeconds(currentCapture.getDuration())
                .monitoringProfile(input.getMonitoringProfiles().isEmpty() ? null :
                        input.getMonitoringProfiles().get(0).getMonitorProfile())
                .captureProfile(currentCapture.getScheduleDetails().getCaptureParameters().getCaptureProfile().getName())
                .isCaptureDetailsPresent(currentCapture.getScheduleDetails().getType() != null)
                .state(currentCapture.getState())
                .build();
    }
}
