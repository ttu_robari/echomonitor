package ee.taltech.bll.mapper;

import ee.taltech.domain.Schedule;
import ee.taltech.domain.ScheduleRow;
import ee.taltech.dto.ScheduleDto;

import java.util.ArrayList;
import java.util.List;

import static ee.taltech.bll.helper.TimeZoneAdjustment.covertToSystemTimeOffset;

public class ScheduleMapper {

    private ScheduleMapper() {
        throw new IllegalStateException("Utility class");
    }

    public static Schedule mapScheduleDto(ScheduleDto scheduleDto) {
        List<ScheduleRow> scheduleRows = new ArrayList<>();
        scheduleDto.getScheduleRows().forEach(row -> scheduleRows.add(ScheduleRow.builder()
                .startTime(covertToSystemTimeOffset(row.getStartTime()))
                .description(row.getStep().getCaptureParameters().getTitle())
                .build()));
        return Schedule.builder().scheduleRows(scheduleRows).build();
    }
}
