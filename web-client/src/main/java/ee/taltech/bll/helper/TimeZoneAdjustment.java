package ee.taltech.bll.helper;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

public class TimeZoneAdjustment {

    private TimeZoneAdjustment() {
        throw new IllegalStateException("Utility class");
    }

    public static LocalDateTime covertToSystemTimeOffset(ZonedDateTime input) {
        return input.withZoneSameInstant(ZonedDateTime.now().getZone()).toLocalDateTime();
    }
}
