package ee.taltech.dto;

import ee.taltech.dto.components.CaptureProfileDto;
import ee.taltech.dto.components.CurrentCaptureDto;
import ee.taltech.dto.components.MonitoringProfileDto;
import ee.taltech.dto.components.ScheduledCaptureDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "status")
@XmlAccessorType(XmlAccessType.FIELD)
public class CaptureStatusDto {
    @XmlElement(name = "wall-clock-time")
    @XmlJavaTypeAdapter(value = ZonedDateTimeAdapter.class)
    private ZonedDateTime wallClockStatus;
    @XmlElement(name = "max-capture-duration-minutes")
    private Integer maxCaptureDurationInMinutes;
    @XmlElement(name = "max-live-capture-extend-duration-minutes")
    private Integer maxLiveCaptureExtendDurationInMinutes;
    @XmlElementWrapper(name = "capture-profiles")
    @XmlElement(name = "capture-profile")
    private List<CaptureProfileDto> captureProfiles = new ArrayList<>();
    @XmlElementWrapper(name = "monitor-profiles")
    @XmlElement(name = "monitor-profile")
    private List<MonitoringProfileDto> monitoringProfiles = new ArrayList<>();
    @XmlElement(name = "next")
    private ScheduledCaptureDto nextCapture = new ScheduledCaptureDto();
    @XmlElement(name = "current")
    private CurrentCaptureDto currentCapture = new CurrentCaptureDto();
}
