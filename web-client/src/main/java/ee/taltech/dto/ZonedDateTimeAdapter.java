package ee.taltech.dto;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class ZonedDateTimeAdapter extends XmlAdapter<String, ZonedDateTime> {

    private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ISO_INSTANT;

    @Override
    public String marshal(ZonedDateTime dateTime) {
        return dateTime.format(DATE_FORMAT);
    }

    @Override
    public ZonedDateTime unmarshal(String dateTime) {
        return ZonedDateTime.parse(dateTime);
    }
}