package ee.taltech.dto.components;

import ee.taltech.dto.ZonedDateTimeAdapter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.ZonedDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "current")
@XmlAccessorType(XmlAccessType.FIELD)
public class CurrentCaptureDto {
    @XmlElement(name = "schedule")
    private CurrentCaptureScheduleDto scheduleDetails = new CurrentCaptureScheduleDto();
    @XmlElement(name = "state")
    private String state;
    @XmlElement(name = "start-time")
    @XmlJavaTypeAdapter(value = ZonedDateTimeAdapter.class)
    private ZonedDateTime startTime;
    @XmlElement(name = "duration")
    private Integer duration;
    @XmlElement(name = "original-duration")
    private Integer originalDuration;
    @XmlElement(name = "confidence-monitoring")
    private Boolean isMonitoring;
    @XmlElement(name = "end-time-limit")
    @XmlJavaTypeAdapter(value = ZonedDateTimeAdapter.class)
    private ZonedDateTime endTimeLimit;
    @XmlElementWrapper(name = "sources")
    @XmlElement(name = "source")
    private List<SourceDto> sourceDtoList;
}
