package ee.taltech.dto.components;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.*;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "sources")
@XmlAccessorType(XmlAccessType.FIELD)
public class SourceDto {
    @XmlElement(name = "class")
    private String sourceClass;
    @XmlElement(name = "subclass")
    private String subclass;
    @XmlElement(name = "name")
    private String name;
    @XmlElement(name = "format")
    private String format;
    @XmlElement(name = "signal-present")
    private Boolean isSignalPresent;
    @XmlElement(name = "supported")
    private Boolean isSupported;
    @XmlElement(name = "thumbnail")
    private String thumbnail;
    @XmlElementWrapper(name = "channels")
    @XmlElement(name = "channel")
    private List<ChannelDto> channels;
}
