package ee.taltech.dto.components;

import ee.taltech.dto.ZonedDateTimeAdapter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.ZonedDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "next")
@XmlAccessorType(XmlAccessType.FIELD)
public class ScheduledCaptureDto {
    @XmlElement(name = "type")
    private String type;
    @XmlElement(name = "start-time")
    @XmlJavaTypeAdapter(value = ZonedDateTimeAdapter.class)
    private ZonedDateTime startTime;
    @XmlElement(name = "duration")
    private Integer duration;
    @XmlElement(name = "parameters")
    private ScheduledCaptureParametersDto captureParameters = new ScheduledCaptureParametersDto();
}
