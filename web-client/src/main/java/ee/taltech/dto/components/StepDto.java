package ee.taltech.dto.components;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "step")
@XmlAccessorType(XmlAccessType.FIELD)
public class StepDto {
    @XmlElement(name = "parameters")
    private ScheduledCaptureParametersDto captureParameters;
}
