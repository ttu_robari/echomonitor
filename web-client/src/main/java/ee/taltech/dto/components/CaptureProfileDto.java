package ee.taltech.dto.components;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "capture-profile")
@XmlAccessorType(XmlAccessType.FIELD)
public class CaptureProfileDto {
    @XmlElement(name = "name")
    private String name;
    @XmlElement(name = "quality")
    private String quality;
    @XmlElement(name = "output-type")
    private String outputType;
}
