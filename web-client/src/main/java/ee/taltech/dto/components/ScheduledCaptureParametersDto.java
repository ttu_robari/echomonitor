package ee.taltech.dto.components;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "parameters")
@XmlAccessorType(XmlAccessType.FIELD)
public class ScheduledCaptureParametersDto {
    @XmlElement(name = "title")
    private String title;
    @XmlElement(name = "section")
    private String section;
    @XmlElementWrapper(name = "presenters")
    @XmlElement(name = "presenter")
    private List<PresenterDto> presenters = new ArrayList<>();
    @XmlElement(name = "capture-profile")
    private CaptureProfileDto captureProfile = new CaptureProfileDto();
}
