package ee.taltech.spring;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ResourceBundle;

@Component
public class StageInitializer implements ApplicationListener<StageReadyEvent> {
    @Value("classpath:ee/taltech/gui/fxml/menu-view.fxml")
    private Resource resource;
    @Value("classpath:messages.properties")
    private Resource locale;
    @Value("${full-screen}")
    private boolean isFullScreen;
    private final String applicationTitle;
    private final ApplicationContext applicationContext;
    private final ResourceBundle resourceBundle;

    public StageInitializer(@Value("${gui.application.title}") String applicationTitle, ApplicationContext applicationContext, ResourceBundle resourceBundle) {
        this.applicationTitle = applicationTitle;
        this.applicationContext = applicationContext;
        this.resourceBundle = resourceBundle;
    }

    @Override
    public void onApplicationEvent(StageReadyEvent event) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(resource.getURL(), resourceBundle);
            fxmlLoader.setControllerFactory(applicationContext::getBean);
            Parent parent = fxmlLoader.load();

            Stage stage = event.getStage();
            stage.setScene(new Scene(parent, 800, 480));
            stage.setMinHeight(480);
            stage.setMinWidth(800);
            stage.setFullScreen(isFullScreen);
            stage.setTitle(applicationTitle);
            stage.show();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
