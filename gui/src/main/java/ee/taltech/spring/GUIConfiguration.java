package ee.taltech.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ResourceBundle;

@Configuration
public class GUIConfiguration {
    @Bean
    public ResourceBundle resourceBundle() {
        return ResourceBundle.getBundle("messages");
    }
}
