package ee.taltech.gui.controller.helper;

import ee.taltech.bll.service.ScheduleService;
import ee.taltech.domain.Schedule;
import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ScheduleClient extends ScheduledService<Schedule> {

    private final ScheduleService scheduleService;
    private final Pane scheduleBody;
    private final Label statusLabel;
    private Pane loadingOverlay;

    public ScheduleClient(ScheduleService scheduleService, Pane scheduleBody, Label statusLabel) {
        this.scheduleService = scheduleService;
        this.scheduleBody = scheduleBody;
        this.statusLabel = statusLabel;
    }

    @Override
    protected Task<Schedule> createTask() {
        return new Task<Schedule>() {
            @Override
            protected Schedule call() {
                try {
                    return scheduleService.getSchedule();
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new RuntimeException(e);
                }

            }
        };
    }

    @Override
    protected void running() {
        super.running();
        statusLabel.setText("Schedule request RUNNING");
        if (scheduleBody.lookup("#loading") == null) {
            loadingOverlay = CommonActions.createLoadingOverlay(false);
            scheduleBody.getChildren().add(loadingOverlay);
        }
    }

    @Override
    protected void succeeded() {
        super.succeeded();
        log.info("Received schedule");
        statusLabel.setText("Schedule request SUCCEEDED");
        scheduleBody.getChildren().remove(loadingOverlay);
        this.cancel();
    }

    @Override
    protected void failed() {
        super.failed();
//        log.error(getMessage());
//        System.out.println(this.getException().getMessage());
        statusLabel.setText("Schedule request FAILED");
    }
}
