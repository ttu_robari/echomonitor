package ee.taltech.gui.controller;

import ee.taltech.bll.service.NextCaptureService;
import ee.taltech.bll.service.PostCommandService;
import ee.taltech.bll.service.ScheduleService;
import ee.taltech.domain.CaptureParameters;
import ee.taltech.domain.CaptureStatus;
import ee.taltech.domain.PostCommand;
import ee.taltech.domain.ScheduleRow;
import ee.taltech.gui.controller.helper.NextCaptureClient;
import ee.taltech.gui.controller.helper.PostCommandClient;
import ee.taltech.gui.controller.helper.ScheduleClient;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.Separator;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.util.Duration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.ResourceBundle;

import static ee.taltech.gui.controller.helper.CommonActions.*;

@Component
@Slf4j
public class MenuViewController {

    @Value("${web-client.capture.ad-hoc.name}")
    private String adHocCaptureName;
    @Value("${web-client.capture.ad-hoc.duration.seconds}")
    private Integer adHocCaptureDuration;
    private String adHocCaptureProfile;

    private ScheduleClient scheduleClient;
    private NextCaptureClient nextCaptureClient;

    private final ApplicationContext applicationContext;
    private final NextCaptureService nextCaptureService;
    private final PostCommandService postCommandService;
    private final ScheduleService scheduleService;
    private final ResourceBundle resourceBundle;

    private Timeline clock;

    @Value("classpath:/ee/taltech/gui/fxml/capture-view.fxml")
    private Resource captureView;
    @Value("classpath:/ee/taltech/gui/fxml/settings-view.fxml")
    private Resource settingsView;

    @FXML
    private Label dateTimeLabel;
    @FXML
    private Label captureNameLabel;
    @FXML
    private Label timeLeftLabel;
    @FXML
    private Label statusLabel;
    @FXML
    private Button startAdhocCaptureButton;
    @FXML
    private Button startScheduledCaptureButton;
    @FXML
    private Circle statusLight;
    @FXML
    private StackPane scheduleBody;
    @FXML
    private VBox scheduleRowContainer;

    private static final Double SCHEDULE_ROW_FONT_SIZE = 23.0;
    private static final Double SCHEDULE_ROW_SPACING = 10.0;

    private java.time.Duration timeUntilNextCapture;

    public MenuViewController(ApplicationContext applicationContext, PostCommandService postCommandService, ScheduleService scheduleService, NextCaptureService nextCaptureService, ResourceBundle resourceBundle) {
        this.applicationContext = applicationContext;
        this.nextCaptureService = nextCaptureService;
        this.postCommandService = postCommandService;
        this.scheduleService = scheduleService;
        this.resourceBundle = resourceBundle;
    }

    @FXML
    private void startAdhocCapture() {
        adHocCaptureProfile="Audio/Dvi-1/Dvi-2 (High Quality)";
        CaptureParameters parameters = new CaptureParameters(adHocCaptureName, adHocCaptureProfile, adHocCaptureDuration);
        PostCommandClient newCapture = new PostCommandClient(postCommandService, PostCommand.NEW_CAPTURE, parameters, statusLabel);

        newCapture.setOnRunning(event -> startAdhocCaptureButton.setGraphic(new ProgressIndicator(ProgressIndicator.INDETERMINATE_PROGRESS)));
        newCapture.setOnSucceeded(event -> {
            startAdhocCaptureButton.setGraphic(null);
        });
        newCapture.setOnFailed(workerStateEvent -> {
            startAdhocCaptureButton.setGraphic(null);
        });
        newCapture.setRestartOnFailure(false);
        newCapture.restart();
    }

    @FXML
    private void startScheduledCapture() {
        CaptureParameters parameters = new CaptureParameters(adHocCaptureName, adHocCaptureProfile, adHocCaptureDuration);
        PostCommandClient newCapture = new PostCommandClient(postCommandService, PostCommand.RECORD, parameters, statusLabel);
        newCapture.setOnRunning(event -> startScheduledCaptureButton.setGraphic(new ProgressIndicator(ProgressIndicator.INDETERMINATE_PROGRESS)));
        newCapture.setOnSucceeded(event -> {
            startScheduledCaptureButton.setGraphic(null);
            changeViewTo(captureView);
        });
        newCapture.setOnFailed(workerStateEvent -> {
            startScheduledCaptureButton.setGraphic(null);
        });
        newCapture.setRestartOnFailure(false);
        newCapture.restart();
    }

    private void stopStageProcesses() {
        clearAnimation(clock);
        scheduleClient.cancel();
        nextCaptureClient.cancel();
    }

    private void changeViewTo(Resource newView) {
        stopStageProcesses();
        loadNewView(newView, resourceBundle, scheduleBody, applicationContext);
    }

    @FXML
    private void loadSettingsView() {
        changeViewTo(settingsView);
    }

//    private void initSyncWithEchoAppliance() {
//        monitoringStatusClient = new MonitoringStatusClient(monitoringStatusService, statusLabel);
//        monitoringStatusClient.setOnSucceeded(event -> {
//            statusLabel.setText(monitoringStatusClient.getValue().getState());
//            statusLight.getStyleClass().clear();
//            statusLight.getStyleClass().add("dot-green");
//            if (!monitoringStatusClient.getValue().getState().equals("inactive")) {
//                changeViewTo(captureView);
//            }
//        });
//        monitoringStatusClient.setOnFailed(event -> {
//            startAdhocCaptureButton.setDisable(true);
//            statusLight.getStyleClass().clear();
//            statusLight.getStyleClass().add("dot-red");
//            if (!scheduleClient.isRunning()) {
//                scheduleClient.restart();
//            }
//            if (!nextCaptureClient.isRunning()) {
//                nextCaptureClient.restart();
//            }
//        });
//        monitoringStatusClient.setPeriod(Duration.seconds(3));
//        monitoringStatusClient.restart();
//    }

    private void initCaptureStatus() {
        nextCaptureClient = new NextCaptureClient(nextCaptureService, statusLabel);
        nextCaptureClient.setOnSucceeded(event -> {
            CaptureStatus captureStatus = nextCaptureClient.getValue();
            String currentState = captureStatus.getState();
            adHocCaptureProfile = captureStatus.getNextCaptureProfile();
            if ("active".equals(currentState) && Boolean.TRUE.equals(captureStatus.getIsCaptureDetailsPresent()) ||
                    "paused".equals(currentState) && Boolean.TRUE.equals(captureStatus.getIsCaptureDetailsPresent())) {
                changeViewTo(captureView);
            } else if ("active".equals(currentState) && !captureStatus.getIsCaptureDetailsPresent()) {
                startAdhocCaptureButton.setDisable(true);
            } else if ("waiting".equals(currentState) && captureStatus.getIsCaptureDetailsPresent()) {
                toggleButton(false, startAdhocCaptureButton);
                toggleButton(true, startScheduledCaptureButton);
                timeLeftLabel.setText(durationToString(java.time.Duration.between(LocalDateTime.now(), captureStatus.getCurrentCaptureStartTime())));
                captureNameLabel.setText(captureStatus.getCurrentCaptureTitle());
            } else {
                if (captureStatus.getNextCaptureStartTime() == null) {
                    timeLeftLabel.setText("");
                } else {
                    timeUntilNextCapture = java.time.Duration.between(LocalDateTime.now(), captureStatus.getNextCaptureStartTime());
                    timeLeftLabel.setText(durationToString(timeUntilNextCapture));
                }
                if (Boolean.FALSE.equals(captureStatus.getIsCaptureDetailsPresent())) {
                    if ("inactive".equals(currentState) || currentState == null) {
                        toggleButton(true, startAdhocCaptureButton);
                        toggleButton(false, startScheduledCaptureButton);
                        startAdhocCaptureButton.setDisable(false);
                    } else {
                        toggleButton(true, startAdhocCaptureButton);
                        toggleButton(false, startScheduledCaptureButton);
                        startAdhocCaptureButton.setDisable(true);
                    }
                }
            }
            captureNameLabel.setText(captureStatus.getNextCaptureTitle());
        });
        nextCaptureClient.setOnFailed(event -> {
            toggleButton(true, startAdhocCaptureButton);
            toggleButton(false, startScheduledCaptureButton);
            startAdhocCaptureButton.setDisable(true);
            timeLeftLabel.setText("");
            captureNameLabel.setText("Loading...");
        });
        nextCaptureClient.setPeriod(Duration.seconds(1));
        nextCaptureClient.restart();
    }

    private void initSchedule() {
        scheduleClient = new ScheduleClient(scheduleService, scheduleBody, statusLabel);
        scheduleClient.setOnSucceeded(event -> {
            scheduleRowContainer.getChildren().clear();
            scheduleClient.getValue().getScheduleRows().forEach(this::buildScheduleRow);
        });
        scheduleClient.setPeriod(Duration.seconds(1));
        scheduleClient.restart();
    }

    private void buildScheduleRow(ScheduleRow row) {
        HBox scheduleRow = new HBox();
        scheduleRow.alignmentProperty().setValue(Pos.CENTER_LEFT);
        scheduleRow.setPadding(new Insets(SCHEDULE_ROW_SPACING));
        scheduleRow.setSpacing(SCHEDULE_ROW_SPACING);
        scheduleRow.getStyleClass().add("bg-dark-blue-shadow");
        scheduleRow.setFillHeight(true);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");

        Label startTimeLabel = new Label(row.getStartTime().format(formatter));
        startTimeLabel.setFont(Font.font(SCHEDULE_ROW_FONT_SIZE));
        startTimeLabel.setMinWidth(Region.USE_PREF_SIZE);
        startTimeLabel.setPadding(new Insets(0, 0, 0, 10));

        Separator separator = new Separator(Orientation.VERTICAL);
        separator.setPadding(new Insets(0, 10, 0, 10));

        Label descriptionLabel = new Label(row.getDescription());
        descriptionLabel.setWrapText(true);
        descriptionLabel.setFont(Font.font(SCHEDULE_ROW_FONT_SIZE));

        scheduleRow.getChildren().addAll(Arrays.asList(startTimeLabel, separator, descriptionLabel));
        scheduleRowContainer.getChildren().add(scheduleRow);
    }

    @FXML
    public void initialize() {
        startAdhocCaptureButton.setDisable(true);
        toggleButton(false, startScheduledCaptureButton);
        clock = initClock(dateTimeLabel);
        initSchedule();
        initCaptureStatus();
    }
}
