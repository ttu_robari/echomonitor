package ee.taltech.gui.controller.helper;

import ee.taltech.bll.service.NextCaptureService;
import ee.taltech.domain.CaptureStatus;
import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;
import javafx.scene.control.Label;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class NextCaptureClient extends ScheduledService<CaptureStatus> {

    private final NextCaptureService nextCaptureService;
    private final Label statusLabel;

    public NextCaptureClient(NextCaptureService nextCaptureService, Label statusLabel) {
        this.nextCaptureService = nextCaptureService;
        this.statusLabel = statusLabel;
    }

    @Override
    protected Task<CaptureStatus> createTask() {
        return new Task<CaptureStatus>() {
            @Override
            protected CaptureStatus call() {
                try {
                    return nextCaptureService.getNextCapture();
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new RuntimeException(e);
                }
            }
        };
    }

    @Override
    protected void succeeded() {
        super.succeeded();
        log.info("Received capture status");
    }

    @Override
    protected void failed() {
        super.failed();
//        log.error(getOnFailed().toString());
//        getException().printStackTrace();
//        System.out.println(getException());
        statusLabel.setText("NextCapture request FAILED");
    }
}
