package ee.taltech.gui.controller.helper;

import ee.taltech.bll.service.MonitoringStatusService;
import ee.taltech.domain.MonitoringStatus;
import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;
import javafx.scene.control.Label;

public class MonitoringStatusClient extends ScheduledService<MonitoringStatus> {

    private final MonitoringStatusService monitoringStatusService;
    private final Label statusLabel;

    public MonitoringStatusClient(MonitoringStatusService monitoringStatusService, Label statusLabel) {
        this.monitoringStatusService = monitoringStatusService;
        this.statusLabel = statusLabel;
    }

    @Override
    protected Task<MonitoringStatus> createTask() {
        return new Task<MonitoringStatus>() {
            @Override
            protected MonitoringStatus call() throws InterruptedException {
                updateMessage("Requesting monitoring status");
                return monitoringStatusService.getMonitoringStatus();
            }
        };
    }

    @Override
    protected void failed() {
        super.failed();
        statusLabel.setText("MonitoringStatus request FAILED");
    }
}
