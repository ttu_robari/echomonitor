package ee.taltech.gui.controller.helper;

import ee.taltech.bll.service.PostCommandService;
import ee.taltech.domain.CaptureParameters;
import ee.taltech.domain.PostCommand;
import ee.taltech.domain.PostResponse;
import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;
import javafx.scene.control.Label;

public class PostCommandClient extends ScheduledService<PostResponse> {

    private final PostCommandService postCommandService;
    private final PostCommand command;
    private final Label statusLabel;
    private CaptureParameters parameters;

    public PostCommandClient(PostCommandService postCommandService, PostCommand command, Label statusLabel) {
        this.postCommandService = postCommandService;
        this.command = command;
        this.statusLabel = statusLabel;
    }

    public PostCommandClient(PostCommandService postCommandService, PostCommand command, CaptureParameters parameters, Label statusLabel) {
        this.postCommandService = postCommandService;
        this.command = command;
        this.statusLabel = statusLabel;
        this.parameters = parameters;
    }

    @Override
    protected Task<PostResponse> createTask() {
        return new Task<PostResponse>() {
            @Override
            protected PostResponse call() {
                switch (command) {
                    case STOP:
                        return postCommandService.stopCapture();
                    case PAUSE:
                        return postCommandService.pauseCapture();
                    case RECORD:
                        return postCommandService.recordCapture();
                    case NEW_CAPTURE:
                        if (parameters != null) {
                            return postCommandService.newCapture(parameters);
                        }
                        throw new RuntimeException("Command requires parameters");
                    case START_MONITORING:
                        if (parameters != null) {
                            return postCommandService.startMonitoring(parameters);
                        }

                        throw new RuntimeException("Command requires parameters");
                    case EXTEND:
                        if (parameters != null) {
                            return postCommandService.extendCapture(parameters);
                        }
                        throw new RuntimeException("Command requires parameters");
                    default:
                        throw new RuntimeException("No command found");
                }
            }
        };
    }

    @Override
    protected void running() {
        super.running();
        statusLabel.setText(String.format("Command %s RUNNING", command));
    }

    @Override
    protected void succeeded() {
        super.succeeded();
        statusLabel.setText(String.format("Command %s SUCCEEDED", command));
        this.cancel();
    }

    @Override
    protected void failed() {
        super.failed();
        statusLabel.setText(String.format("Command %s FAILED", command));
        System.out.println(this.getException());
    }
}
