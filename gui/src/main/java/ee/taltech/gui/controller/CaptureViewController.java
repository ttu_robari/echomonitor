package ee.taltech.gui.controller;

import ee.taltech.bll.service.InputImageService;
import ee.taltech.bll.service.MonitoringStatusService;
import ee.taltech.bll.service.NextCaptureService;
import ee.taltech.bll.service.PostCommandService;
import ee.taltech.domain.CaptureParameters;
import ee.taltech.domain.CaptureStatus;
import ee.taltech.domain.PostCommand;
import ee.taltech.gui.controller.helper.*;
import javafx.animation.*;
import javafx.concurrent.ScheduledService;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javafx.util.Duration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import javax.sound.sampled.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import static ee.taltech.gui.controller.helper.CommonActions.*;

@Component
@Slf4j
public class CaptureViewController {

    private final ApplicationContext applicationContext;
    private final MonitoringStatusService monitoringStatusService;
    private final InputImageService inputImageService;
    private final PostCommandService postCommandService;
    private final NextCaptureService nextCaptureService;
    private final ResourceBundle resourceBundle;
    private final List<InputImageClient> inputImageClients = new ArrayList<>();
    private NextCaptureClient nextCaptureClient;
    private MonitoringStatusClient monitoringStatusClient;
    private java.time.Duration timeUntilCaptureEnd;
    private Integer currentConsecutiveSilenceAmount = 0;
    private Clip notificationSound;
    private final List<Node> buttons = new ArrayList<>();

    private static final Integer RESET_COUNTER = 0;
    private static final Duration SECONDS_BETWEEN_STATUS_REQUESTS = Duration.seconds(1);
    private static final double VOLUME_BAR_WIDTH = 350;
    private static final double IMAGE_WIDTH_PERCENT = 0.88;
    private static final double IMAGE_HEIGHT_PERCENT = 0.4125;
    private static final Integer HUNDRED_PERCENT = 100;
    private static final Duration SECONDS_BETWEEN_MONITORING_REQUESTS = Duration.seconds(1);
    private static final Duration SECONDS_BETWEEN_IMAGE_REQUESTS = Duration.seconds(2);
    private static final String PAUSE = "pause";
    private static final String INPUT = "input";
    private static final String LOADING = "loading";
    private Timeline clock;

    @Value("classpath:/ee/taltech/gui/fxml/menu-view.fxml")
    private Resource menuViewFxml;
    @Value("classpath:/ee/taltech/gui/resources/notification.wav")
    private Resource audioFile;
    @Value("${capture.audio.silence-threshold}")
    private Integer silenceThreshold;
    @Value("${capture.audio.amount-of-consecutive-silence}")
    private Integer consecutiveSilenceLimit;
    @Value("${capture.audio.maximum-value}")
    private double maxVolume;
    @Value("${web-client.capture.extend.seconds}")
    private Integer captureExtendAmount;

    @FXML
    private Label dateTimeLabel;
    @FXML
    private Label statusLabel;
    @FXML
    private Label captureDescriptionLabel;
    @FXML
    private Label timeLeftLabel;
    @FXML
    private Button stopCaptureButton;
    @FXML
    private Button resumeCaptureButton;
    @FXML
    private Button pauseCaptureButton;
    @FXML
    private Button extendCaptureButton;
    @FXML
    private Pane stopShape;
    @FXML
    private Pane resumeShape;
    @FXML
    private Pane pauseShape;
    @FXML
    private Pane extendShape;
    @FXML
    private Circle statusLight;
    @FXML
    private Pane audioVolumeBar;
    @FXML
    private HBox inputImageContainer;
    @FXML
    private GridPane mainPane;
    @FXML
    private Pane lowVolumeSymbol;

    public CaptureViewController(ApplicationContext applicationContext, MonitoringStatusService monitoringStatusService, InputImageService inputImageService, PostCommandService postCommandService, NextCaptureService nextCaptureService, ResourceBundle resourceBundle) {
        this.applicationContext = applicationContext;
        this.monitoringStatusService = monitoringStatusService;
        this.inputImageService = inputImageService;
        this.postCommandService = postCommandService;
        this.nextCaptureService = nextCaptureService;
        this.resourceBundle = resourceBundle;
    }

    @FXML
    private void pauseCapture() {
        RotateTransition loading = spinNode(pauseShape);
        PostCommandClient pauseRequest = sendCommand(PostCommand.PAUSE);
        pauseShape.getProperties().put(LOADING, loading);

        pauseRequest.setOnRunning(event -> {
            loading.play();
            disableNodes(buttons);
        });
        pauseRequest.setOnSucceeded(event -> enableNodes(buttons));
        pauseRequest.setOnFailed(event -> {
            loading.stop();
            enableNodes(buttons);
        });
    }

    private void createPauseOverlay() {
        for (Node node : inputImageContainer.getChildren()) {
            if (node.lookup("#" + PAUSE) == null) {
                toggleButton(false, pauseCaptureButton);
                toggleButton(true, resumeCaptureButton);
                ((Pane) node).getChildren().add(createOverlayElement());
            }
        }
    }

    private Pane createOverlayElement() {
        Pane roundPause = new Pane();
        roundPause.getStyleClass().add("pause-round-shape");
        roundPause.maxHeightProperty().bind(inputImageContainer.heightProperty().multiply(0.7));
        roundPause.maxWidthProperty().bind(inputImageContainer.heightProperty().multiply(0.7));
        StackPane overlayParent = new StackPane(roundPause);
        overlayParent.getStyleClass().add("bg-black-semitransparent");
        overlayParent.setId(PAUSE);
        return overlayParent;
    }

    @FXML
    private void resumeCapture() {
        RotateTransition loading = spinNode(resumeShape);
        PostCommandClient resumeRequest = sendCommand(PostCommand.RECORD);
        resumeShape.getProperties().put(LOADING, loading);

        resumeRequest.setOnRunning(event -> {
            loading.play();
            disableNodes(buttons);
        });
        resumeRequest.setOnSucceeded(event -> enableNodes(buttons));
        resumeRequest.setOnFailed(event -> {
            loading.stop();
            enableNodes(buttons);
        });
    }

    private void removePauseOverlay() {
        inputImageContainer.getChildren().forEach(element -> {
            if (INPUT.equals(element.getId())) {
                toggleButton(true, pauseCaptureButton);
                toggleButton(false, resumeCaptureButton);
                ((Pane) element).getChildren().remove(element.lookup("#" + PAUSE));
            }
        });
    }

    @FXML
    private void stopCapture() {
        RotateTransition loading = spinNode(stopShape);
        PostCommandClient stopRequest = sendCommand(PostCommand.STOP);

        stopRequest.setOnRunning(event -> {
            loading.play();
            disableNodes(buttons);
        });
        stopRequest.setOnSucceeded(event -> loading.stop());
        stopRequest.setOnFailed(event -> {
            loading.stop();
            enableNodes(buttons);
        });
    }

    @FXML
    private void extendCapture() {
        RotateTransition rotateTransition = spinNode(extendShape);
        PostCommandClient stopRequest = sendCommand(PostCommand.EXTEND);
        stopRequest.setOnRunning(event -> {
            rotateTransition.play();
            disableNodes(buttons);
        });
        stopRequest.setOnSucceeded(event -> {
            rotateTransition.stop();
            enableNodes(buttons);
        });
        stopRequest.setOnFailed(event -> {
            rotateTransition.stop();
            enableNodes(buttons);
        });
    }

    @FXML
    public void initialize() {
        toggleButton(false, resumeCaptureButton);
        clock = initClock(dateTimeLabel);

        buttons.add(extendCaptureButton);
        buttons.add(pauseCaptureButton);
        buttons.add(resumeCaptureButton);
        buttons.add(stopCaptureButton);

        try {
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(audioFile.getURL());
            notificationSound = AudioSystem.getClip();
            notificationSound.open(audioInputStream);
        } catch (LineUnavailableException | IOException | UnsupportedAudioFileException e) {
            log.error("Failed to initialize audio file", e);
        }

        ImageView view = new ImageView();
        StackPane singleInputElement = new StackPane(view);
        Pane loadingOverlay = CommonActions.createLoadingOverlay(true);
        singleInputElement.getChildren().add(loadingOverlay);

        mainPane.sceneProperty().addListener((observableScene, oldScene, newScene) -> {
            if (oldScene == null && newScene != null) {
                setOptimalImageSize(view, 1);
                inputImageContainer.getChildren().add(singleInputElement);
                lowVolumeSymbol.maxWidthProperty().bind(inputImageContainer.heightProperty().multiply(0.9));
                lowVolumeSymbol.maxHeightProperty().bind(inputImageContainer.heightProperty().multiply(0.9));
                lowVolumeSymbol.getParent().setVisible(false);
            }
        });

        initMonitoringStatus();
        initCaptureStatus();
    }

    private PostCommandClient sendCommand(PostCommand command) {
        CaptureParameters parameters = new CaptureParameters(captureExtendAmount);
        PostCommandClient commandClient = new PostCommandClient(postCommandService, command, parameters, statusLabel);
        commandClient.setRestartOnFailure(false);
        commandClient.setPeriod(Duration.seconds(1));
        commandClient.setMaximumFailureCount(1);
        commandClient.start();

        return commandClient;
    }

    private void loadMainMenu() {
        clearAnimation(clock);
        inputImageClients.forEach(ScheduledService::cancel);
        inputImageClients.clear();
        monitoringStatusClient.cancel();
        nextCaptureClient.cancel();
        currentConsecutiveSilenceAmount = 0;

        loadNewView(menuViewFxml, resourceBundle, mainPane, applicationContext);
    }

    private void animateAudioVolumeBar(double volume) {
        double loudnessPercentage = volume * HUNDRED_PERCENT / maxVolume;
        double calculatedBarPosition = VOLUME_BAR_WIDTH * loudnessPercentage / HUNDRED_PERCENT;

        Duration cycleDuration = Duration.millis(750);
        Timeline timeline = new Timeline(
                new KeyFrame(cycleDuration,
                        new KeyValue(audioVolumeBar.maxWidthProperty(), calculatedBarPosition, Interpolator.EASE_BOTH))
        );
        timeline.play();
    }

    private void setOptimalImageSize(ImageView image, Integer imageCount) {
        Stage currentStage = (Stage) mainPane.getScene().getWindow();
        image.fitWidthProperty().bind(currentStage.widthProperty().divide(imageCount).multiply(IMAGE_WIDTH_PERCENT));
        image.fitHeightProperty().bind(currentStage.heightProperty().multiply(IMAGE_HEIGHT_PERCENT));
    }

    private void initCaptureStatus() {
        nextCaptureClient = new NextCaptureClient(nextCaptureService, statusLabel);
        nextCaptureClient.setOnSucceeded(event -> {
            CaptureStatus captureStatus = nextCaptureClient.getValue();
            if (Boolean.TRUE.equals(captureStatus.getIsCaptureDetailsPresent())) {

                LocalDateTime currentTime = LocalDateTime.now();
                LocalDateTime captureStartTime = captureStatus.getCurrentCaptureStartTime();
                Integer captureDuration = captureStatus.getCurrentCaptureDurationSeconds();
                timeUntilCaptureEnd = java.time.Duration.between(currentTime, captureStartTime.plusSeconds(captureDuration));
                timeLeftLabel.setText(durationToString(timeUntilCaptureEnd));
                captureDescriptionLabel.setText(captureStatus.getCurrentCaptureTitle());
            } else {
                loadMainMenu();
            }
        });
        nextCaptureClient.setPeriod(SECONDS_BETWEEN_STATUS_REQUESTS);
        nextCaptureClient.restart();
    }

    private void initMonitoringStatus() {
        monitoringStatusClient = new MonitoringStatusClient(monitoringStatusService, statusLabel);
        monitoringStatusClient.setPeriod(SECONDS_BETWEEN_MONITORING_REQUESTS);
        monitoringStatusClient.setOnSucceeded(event -> {
            log.info(monitoringStatusClient.getValue().getState());
            if (monitoringStatusClient.getValue().getState().equals("active") && inputImageClients.isEmpty() ||
                    monitoringStatusClient.getValue().getState().equals("paused") && inputImageClients.isEmpty()) {
                inputImageContainer.getChildren().clear();
                monitoringStatusClient.getValue().getThumbnailImageFileNames().forEach(image -> {
                    ImageView view = new ImageView();
                    StackPane singleInputElement = new StackPane(view);
                    singleInputElement.setId(INPUT);
                    Pane loadingOverlay = CommonActions.createLoadingOverlay(true);
                    singleInputElement.getChildren().add(loadingOverlay);

                    view.setSmooth(true);
                    view.setPreserveRatio(true);

                    setOptimalImageSize(view, monitoringStatusClient.getValue().getThumbnailImageFileNames().size());
                    inputImageContainer.getChildren().add(singleInputElement);

                    InputImageClient imageClient = new InputImageClient(inputImageService, image);
                    imageClient.setOnSucceeded(event1 -> {
                        Image inputImage = new Image(new ByteArrayInputStream(imageClient.getValue()));
                        view.setImage(inputImage);
                        singleInputElement.getChildren().remove(loadingOverlay);
                    });
                    imageClient.setOnFailed(event1 -> {
                        if (!singleInputElement.getChildren().contains(loadingOverlay)) {
                            singleInputElement.getChildren().add(loadingOverlay);
                        }
                        statusLabel.setText("Failed to get image");
                    });
                    imageClient.setPeriod(SECONDS_BETWEEN_IMAGE_REQUESTS);
                    imageClient.setRestartOnFailure(true);
                    imageClient.start();
                    inputImageClients.add(imageClient);
                });
            }
            if (monitoringStatusClient.getValue().getState().equals("active")) {
                processAudioVolume(monitoringStatusClient.getValue().getVolume());
                if (resumeShape.hasProperties() && resumeShape.getProperties().get(LOADING) != null) {
                    ((RotateTransition) resumeShape.getProperties().get(LOADING)).stop();
                }
                removePauseOverlay();
            }
            if (monitoringStatusClient.getValue().getState().equals("paused")) {
                if (pauseShape.hasProperties() && pauseShape.getProperties().get(LOADING) != null) {
                    ((RotateTransition) pauseShape.getProperties().get(LOADING)).stop();
                }
                createPauseOverlay();
            }
        });
        monitoringStatusClient.setOnFailed(event -> {
            statusLabel.setText("Updating monitoring status");
            statusLight.getStyleClass().clear();
            statusLight.getStyleClass().add("dot-red");
            nextCaptureClient.restart();
        });
        monitoringStatusClient.restart();
    }

    private void processAudioVolume(Double volume) {
        animateAudioVolumeBar(volume);
        checkRecentVolumeLoudness(volume);
    }

    private void checkRecentVolumeLoudness(Double volume) {
        if (volume > silenceThreshold) {
            currentConsecutiveSilenceAmount = RESET_COUNTER;
            showLowVolumeWarning(false);
            notificationSound.stop();
            notificationSound.setMicrosecondPosition(0);
        } else {
            currentConsecutiveSilenceAmount++;
        }
        if (currentConsecutiveSilenceAmount >= (consecutiveSilenceLimit)) {
            showLowVolumeWarning(true);
            notificationSound.start();
        }
    }

    private void showLowVolumeWarning(boolean b) {
        lowVolumeSymbol.getParent().setVisible(b);
    }
}
