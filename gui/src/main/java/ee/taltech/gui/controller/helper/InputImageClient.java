package ee.taltech.gui.controller.helper;

import ee.taltech.bll.service.InputImageService;
import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;
import org.springframework.stereotype.Component;

public class InputImageClient extends ScheduledService<byte[]> {

    private final InputImageService inputImageService;
    private final String imageName;

    public InputImageClient(InputImageService inputImageService, String imageName) {
        this.inputImageService = inputImageService;
        this.imageName = imageName;
    }

    @Override
    protected Task<byte[]> createTask() {
        return new Task<byte[]>() {
            @Override
            protected byte[] call() {
                return inputImageService.getThumbnailImage(imageName);
            }
        };
    }
}
