package ee.taltech.gui.controller;

import ee.taltech.gui.controller.helper.CommonActions;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.util.ResourceBundle;

@Component
public class SettingsViewController {

    private final ApplicationContext applicationContext;
    private final ResourceBundle resourceBundle;

    @Value("classpath:/ee/taltech/gui/fxml/menu-view.fxml")
    private Resource resource;

    @FXML
    private Button exitApplicationButton;

    public SettingsViewController(ApplicationContext applicationContext, ResourceBundle resourceBundle) {
        this.applicationContext = applicationContext;
        this.resourceBundle = resourceBundle;
    }

    @FXML
    private void exitApplication() {
        Platform.exit();
    }

    @FXML
    private void startMonitoring() {
        Platform.exit();
    }

    @FXML
    private void loadMainMenuView() {
        CommonActions.loadNewView(resource, resourceBundle,exitApplicationButton, applicationContext);
    }

    @FXML
    public void initialize() {
    }
}
