package ee.taltech.gui.controller.helper;

import javafx.animation.*;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.transform.Rotate;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.ResourceBundle;

public class CommonActions {

    private static final String LOADING = "loading";

    public static void toggleButton(boolean isVisible, Button button) {
        button.setVisible(isVisible);
        button.setManaged(isVisible);
    }

    public static void disableNodes(List<Node> nodes) {
        nodes.forEach(node -> node.setDisable(true));
    }

    public static void enableNodes(List<Node> nodes) {
        nodes.forEach(node -> node.setDisable(false));
    }

    public static String durationToString(java.time.Duration timeLeft) {
        String result;
        if (timeLeft.isNegative()) {
            result = "-" + DurationFormatUtils.formatDuration(timeLeft.negated().toMillis(), "H'h' mm'm' ss's'", false);
        } else {
            result = DurationFormatUtils.formatDuration(timeLeft.toMillis(), "H'h' mm'm' ss's'", false);
        }
        return result;
    }

    public static Timeline initClock(Label dateTimeLabel) {
        Timeline clock = new Timeline(new KeyFrame(Duration.ZERO, e -> {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yy HH:mm");
            dateTimeLabel.setText(LocalDateTime.now().format(formatter));
        }), new KeyFrame(Duration.seconds(1)));
        clock.setCycleCount(Animation.INDEFINITE);
        clock.play();
        return clock;
    }

    public static Pane createLoadingOverlay(boolean shouldDimBackground) {
        Pane loadingShape = new Pane();
        loadingShape.setMaxHeight(80);
        loadingShape.setMaxWidth(60);
        loadingShape.getStyleClass().add("loading-shape");
        StackPane parent = new StackPane(loadingShape);
        spinNode(loadingShape);
        if (shouldDimBackground) {
            parent.getStyleClass().add("bg-black-semitransparent");
        }
        RotateTransition loading = spinNode(loadingShape);
        parent.getProperties().put(LOADING, loading);
        parent.setId(LOADING);
        loading.play();
        return parent;
    }

    public static void clearAnimation(Timeline timeline) {
        if (timeline != null) {
            timeline.stop();
            timeline.getKeyFrames().clear();
        }
    }

    public static RotateTransition spinNode(Node node) {
        RotateTransition rotate = new RotateTransition();
        rotate.setAxis(Rotate.Z_AXIS);
        rotate.setByAngle(360);
        rotate.setInterpolator(Interpolator.EASE_BOTH);
        rotate.setCycleCount(-1);
        rotate.setDuration(Duration.seconds(0.7d));
        rotate.setAutoReverse(false);
        rotate.setNode(node);

        rotate.statusProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue == Animation.Status.STOPPED) {
                RotateTransition recoverAngle = new RotateTransition(Duration.seconds(0.2d), node);
                recoverAngle.setFromAngle(node.getRotate());
                recoverAngle.setToAngle(0);
                recoverAngle.setCycleCount(1);
                recoverAngle.setAutoReverse(true);
                recoverAngle.play();
            }
        });
        return rotate;
    }

    public static void loadNewView(Resource newFxmlView, ResourceBundle locale, Node currentStageElement, ApplicationContext applicationContext) {
        try {
            Stage currentStage = (Stage) currentStageElement.getScene().getWindow();
            FXMLLoader fxmlLoader = new FXMLLoader(newFxmlView.getURL(), locale);
            fxmlLoader.setControllerFactory(applicationContext::getBean);
            Parent newViewParent = fxmlLoader.load();
            currentStage.getScene().setRoot(newViewParent);
            currentStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
